import 'package:cloud_firestore/cloud_firestore.dart';

class FirestoreMedthods {
  
//main() {
//Firestore.instance.settings(persistenceEnabled: true);
//print("PERSISTENCE HIT");
//}

  // get all docs in collection
  getCollection() async {
    return await Firestore.instance.collection('safeCheckDocs').getDocuments();
  }

  // get the ingredients doc
  getIngredients() async {
    return await Firestore.instance.collection('safeCheckDocs').document('ingredients').get();
  }

  // get ingredient types doc
  getTypes() async {
    return await Firestore.instance.collection('safeCheckDocs').document('ingredTypes').get();
  }

  // get diets doc
  getDiets() async {
    return await Firestore.instance.collection('safeCheckDocs').document('standardDiets').get();
  }
}