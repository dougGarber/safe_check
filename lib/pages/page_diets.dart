import 'package:flutter/material.dart';
import '../utility/globals.dart';
import '../models/firestore_model.dart';

class PageDiets extends StatefulWidget {
  @override
  _PageDietsState createState() => _PageDietsState();
}

Record record = new Record();




class _PageDietsState extends State<PageDiets> {

// init
@override
void initState() {
  setState(() {
  record =  Record.fromSnapshot(globals.dietsSnapshot);
  });
  super.initState();
}


  @override
  Widget build(BuildContext context) {
     if (record.glutenFree == null) {
      return Text('Loading, Please wait..');
    } else {
    return Scaffold(
      body: new Container(
        child: new Column (
          children: <Widget>[
            _createAccordion()
            
          ],
        ),
      ),
    );
  }}
}

// Accordion
Widget _createAccordion() {
  return new Flexible(
    child: new ListView.builder(
      itemCount: 2,
      itemBuilder: (context, i) {
        
      },
    )
  );
}

class Record {
  List<Diets> glutenFree = new List<Diets>();
  List<String> dairyFree = new List<String>();
  Record ({
    this.glutenFree,
    this.dairyFree
  });

  Record.fromSnapshot(snapshot)
    : glutenFree = List.from(snapshot['gluten free']).map<Diets>((data) {
      return Diets.fromMap(data);
    }).toList();     
}