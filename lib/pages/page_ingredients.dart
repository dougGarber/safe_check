import 'package:flutter/material.dart';
import '../services/firestore_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/firestore_model.dart';

//   MOVE INTO A THEMES OR GLOBAL VALUES
const appColor = const Color(0xFF799A6B);
const logoColor = const Color(0xFF5C7E4D);
const tabColor = const Color(0xFF9EB993);
const white = const Color(0xFFF7F7F7);
const inactiveIcon = const Color(0xFFC8D8C1);

class PageIngredients extends StatefulWidget {
  @override
  PageIngredientsState createState() => new PageIngredientsState();
}

class PageIngredientsState extends State<PageIngredients> {
  // search
  var _searchview = new TextEditingController();
  bool _firstSearch = true;
  String _query = "";
  List<String> _filterList;

  // firestore
  DocumentSnapshot ingredSnapshot;
  FirestoreMedthods fireObj = FirestoreMedthods();
  Record record = new Record();

  // init
  @override
  void initState() {
    fireObj.getIngredients().then((results) {
      setState(() {
        ingredSnapshot = results;
        record = Record.fromSnapshot(ingredSnapshot);
      });
    });
    super.initState();
  }

  PageIngredientsState() {
    //Register a closure to be called when the object changes.
    _searchview.addListener(() {
      if (_searchview.text.isEmpty) {
        //Notify the framework that the internal state of this object has changed.
        setState(() {
          _firstSearch = true;
          _query = "";
        });
      } else {
        setState(() {
          _firstSearch = false;
          _query = _searchview.text;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (record.items == null) {
      return Text('Loading, Please wait..');
    } else {
      return new Scaffold(
        appBar: _createSearchView(),
        body: new Container(
          child: new Column(
            children: <Widget>[
              _firstSearch ? _createListView() : _performSearch()
            ],
          ),
        ),
      );
    }
  }
  
  // SearchView
  Widget _createSearchView() {
    return new AppBar(
      //decoration: BoxDecoration(border: Border.all(width: 1.0)),
      backgroundColor: tabColor,
      title: new Column(
        children: <Widget>[
          new TextField(
            controller: _searchview,
            decoration: InputDecoration(
              hintText: "Search",
              hintStyle: new TextStyle(color: Colors.grey[300]),
            ),
            textAlign: TextAlign.left,
          )
        ],
      )
    );
  }

  // ListView
  Widget _createListView() {
    return new Flexible(
      child: new ListView.builder(
        itemCount: record.items.length,
        itemBuilder: (context, i) {
          return new ExpansionTile(
            title: new Text(record.items[i].id, style: new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),),
            children: <Widget>[
              new Column(
                children: _buildExpandableContent(record.items[i]),
              ),
            ],
          );
        },
      )
    );
  }

  _buildExpandableContent(Ingredients ingreds) {
    List<Widget> columnContent = [];  
      columnContent.add(
        new ListTile(
          title: new Text(ingreds.names, style: new TextStyle(fontSize: 18.0),),
          //leading: new Icon(vehicle.icon),
        ),
      );
    return columnContent;
  }
 
  // Post Listview after filter
  Widget _createFilteredListView() {
    return new Flexible(
      child: new ListView.builder(
        itemCount: _filterList.length,
        itemBuilder: (BuildContext context, int index) {
          return new Card(
            color: Colors.white,
            elevation: 5.0,
            child: new Container(
              margin: EdgeInsets.all(15.0),
              child: new Text("${_filterList[index]}"),
            ),
          );
        }
      ),
    );
  }

  //Perform actual search
  Widget _performSearch() {
    _filterList = new List<String>();
    for (int i = 0; i < record.items.length; i++) {
      var item = record.items[i].id;
      if (item.toLowerCase().contains(_query.toLowerCase())) {
        _filterList.add(item);
      }
    }
    return _createFilteredListView();
  }
}


class Record {
  List<Ingredients> items = new List<Ingredients>();
  Record ({
    this.items
  });

  Record.fromSnapshot(DocumentSnapshot snapshot)
    : items = List.from(snapshot['items']).map<Ingredients>((data) {
      return Ingredients.fromMap(data);
    }).toList();     
}

//NOTES
//search
//https://camposha.info/course/flutter-widgets/lesson/flutter-searchview-listview-search-filter/
//expandable listview
//https://stackoverflow.com/questions/50530152/how-to-create-expandable-listview-in-flutter