import 'package:flutter/material.dart';
import './page_ingredients.dart';
import './page_types.dart';

//   MOVE INTO A THEMES OR GLOBAL VALUES
const appColor = const Color(0xFF799A6B);
const logoColor = const Color(0xFF5C7E4D);
const tabColor = const Color(0xFF9EB993);
const white = const Color(0xFFF7F7F7);
const inactiveIcon = const Color(0xFFC8D8C1);

class TabIngredients extends StatefulWidget {
  @override
  _TabIngredientsState createState() => _TabIngredientsState();
}

class _TabIngredientsState extends State<TabIngredients>
    with SingleTickerProviderStateMixin {
  TabController tabController;


  @override
  void initState() {
    super.initState();

    tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  Widget getTabBar() {
    return TabBar(indicatorColor: appColor, controller: tabController, tabs: [
      Tab(text: "Ingredients"),
      Tab(text: "Types"),
    ]);
  }

  Widget getTabBarPages() {
    return TabBarView(controller: tabController, children: <Widget>[
      Container(
        child: PageIngredients()
        ),
      Container(
        child: PageTypes()
      ),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: tabColor,
          flexibleSpace: SafeArea(
            child: getTabBar(),
          ),
        ),
        body: getTabBarPages());
  }
}