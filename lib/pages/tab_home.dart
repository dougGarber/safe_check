import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../services/firestore_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/firestore_model.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:math' as math;
import 'package:http/http.dart' as http;
import 'package:xml2json/xml2json.dart';
import 'dart:convert';
import '../models/feed_model.dart';
import 'package:url_launcher/url_launcher.dart';


class TabHome extends StatefulWidget {

  @override
  State createState() => new TabHomeState();
}

class TabHomeState extends State<TabHome> {  
  List types = new List();            // completed list of types
  var typesCnt = Map();               // counts of each type
  List<ChartCnts> data = new List();  // formatted list for chart series
  String totalIngred;                 // total ingredients count
  String totalTypes;                  // total types count
  Xml2Json xml2json = new Xml2Json();

  // firestore objects
  DocumentSnapshot ingredSnapshot;
  FirestoreMedthods fireObj = FirestoreMedthods();
  Record record = new Record();

  // feed object
  List<FeedItems> feedSnapshot;

  // get firestore ingredient data
  getChartData() async{
    return ingredSnapshot =  await fireObj.getIngredients();
  }

  // get xml data feed
  getFeedData() async{
    String url = 'https://www.foodengineeringmag.com/rss/topic/2628-food-safety';
    http.Response response = await http.get(url);
    // parse xml data to json
    xml2json.parse(response.body);
    var jsondata = xml2json.toGData();
    var data = json.decode(jsondata);
    var items = data['rss']['channel']['item'] as List;
    feedSnapshot = items.map<FeedItems>((json) => FeedItems.fromJson(json)).toList();

    return feedSnapshot;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
        Container(
          padding: const EdgeInsets.all(0.0),
          child:
            new Column(
              children: <Widget>[
                new Expanded(
                  child:
                    FutureBuilder(
                      future: getChartData(),
                      builder: (BuildContext context,AsyncSnapshot snapshot){
                
                        if(snapshot.connectionState == ConnectionState.done)   {
                          return new Stack (
                            children: <Widget>[
                              new charts.PieChart(
                                dataList(snapshot.data),
                                defaultRenderer: new charts.ArcRendererConfig(
                                  arcWidth: 50, startAngle: 4 / 5 * math.pi, arcLength: 7 / 5 * math.pi),
                              ),
                              Center(
                                child: Text(totalIngred + '\nIngredients\n\n' + totalTypes + '\nTypes', textAlign: TextAlign.center,)
                              ),
                            ],
                          );
                        }else{
                          return Center(child: CircularProgressIndicator());
                        }
                      }    
                    )
                ),
                new Expanded(
                  child:
                    FutureBuilder(
                      future: getFeedData(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if(snapshot.connectionState == ConnectionState.done) {
                          return createListView(context, snapshot);
                        }
                        else
                        {
                          return Center(child: CircularProgressIndicator());
                        }
                      },
                    )
                )
              ]
            ),
        )
    );
  }

Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
    List values = snapshot.data;
    print(values[0].title);
    return new ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return new Column(
            children: <Widget>[
              new ListTile(
                onTap:(){launchURL(values[index].link.link.toString());},
                leading: new Image.network(values[index].enclosure.enclosure.toString(),fit: BoxFit.cover,),
                title: new Text(values[index].title.title.toString()),
                subtitle: new Text(values[index].description.description.toString()),
              ),
              new Divider(height: 2.0,),
            ],
          );
        },
    );
  }

 launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  List<charts.Series<ChartCnts, String>> dataList(apiData) {
    // format firestore ingredients data
    record = Record.fromSnapshot(apiData);

    // set total ingredients count
    totalIngred = record.items.length.toString();

    // get complete type list
    for(var i=0; i < record.items.length; i++)
      { 
        var hold = record.items[i].cat; 
        types.add(hold);  
      }

    // get type counts
    types.forEach((x) => typesCnt[x] = !typesCnt.containsKey(x) ? (1) : (typesCnt[x] + 1));

    // format type count data for chart
    typesCnt.forEach((k,v) =>
      data.add(new ChartCnts('$k', v))
    );

    // set total types count
    totalTypes = data.length.toString();

    // load chart data    
    return [
      new charts.Series<ChartCnts,String>(
        id: 'types',
        domainFn: (ChartCnts types, _) => types.type,
        measureFn: (ChartCnts types, _) => types.cnt,
        data: data,
      )
    ];
  }
}


class Record {
  List<Ingredients> items = new List<Ingredients>();
  Record ({
    this.items
  });

  Record.fromSnapshot(DocumentSnapshot snapshot)
    : items = List.from(snapshot['items']).map<Ingredients>((data) {
      return Ingredients.fromMap(data);
    }).toList();     
}



