import 'package:flutter/material.dart';
import './page_itemCheck.dart';
import './page_labelCheck.dart';
import './page_diets.dart';
import '../utility/globals.dart';

class TabSafeCheck extends StatefulWidget {
  @override
  _TabSafeCheckState createState() => _TabSafeCheckState();
}

class _TabSafeCheckState extends State<TabSafeCheck>
    with SingleTickerProviderStateMixin {
  TabController tabController;


  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  Widget getTabBar() {
    return TabBar(indicatorColor: globals.appColor, controller: tabController, tabs: [
      Tab(text: "Item Check"),
      Tab(text: "Label Check"),
      Tab(text: "Diets"),
    ]);
  }

  Widget getTabBarPages() {
    return TabBarView(controller: tabController, children: <Widget>[
      Container(
        child: PageItemCheck()
      ),
      Container(
        child: PageLabelCheck()
        ),
      Container(
        child: PageDiets()
      ),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: globals.tabColor,
          flexibleSpace: SafeArea(
            child: getTabBar(),
          ),
        ),
        body: getTabBarPages());
  }
}
