import 'package:flutter/material.dart';
import '../services/firestore_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/firestore_model.dart';


class PageTypes extends StatefulWidget {
  @override
  PageTypesState createState() => new PageTypesState();
}

class PageTypesState extends State<PageTypes> {

  // firestore
  DocumentSnapshot typesSnapshot;
  FirestoreMedthods fireObj = FirestoreMedthods();
  Record record = new Record();

  // init
  @override
  void initState() {
    fireObj.getTypes().then((results) {
      setState(() {
        typesSnapshot = results;
        record = Record.fromSnapshot(typesSnapshot);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (record.types == null) {
      return Text('Loading, Please wait..');
    } else {
      return new Scaffold(
        body: new Container(
          child: new Column(
            children: <Widget>[
              _createListView()
            ],
          ),
        ),
      );
    }
  }
  
  // ListView
  Widget _createListView() {
    return new Flexible(
      child: new ListView.builder(
        itemCount: record.types.length,
        itemBuilder: (context, i) {
          return new ExpansionTile(
            title: new Text(record.types[i].name, style: new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),),
            children: <Widget>[
              new Column(
                children: _buildExpandableContent(record.types[i]),
              ),
            ],
          );
        },
      )
    );
  }

  _buildExpandableContent(Types types) {
    List<Widget> columnContent = [];  
      columnContent.add(
        new ListTile(
          title: new Text(types.id, style: new TextStyle(fontSize: 18.0),),
          //leading: new Icon(vehicle.icon),
        ),
      );
    return columnContent;
  }
}

class Record {
  List<Types> types = new List<Types>();
  Record ({
    this.types
  });

  Record.fromSnapshot(DocumentSnapshot snapshot)
    : types = List.from(snapshot['types']).map<Types>((data) {
      return Types.fromMap(data);
    }).toList();     
}