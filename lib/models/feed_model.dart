import 'package:html/parser.dart';

// main page data feed from https://www.foodengineeringmag.com/rss/topic/2628-food-safety
class FeedItems {
  dynamic title;
  dynamic author;
  dynamic description;
  dynamic guid;
  dynamic pubDate;
  dynamic link;
  dynamic enclosure;

  FeedItems ({
    this.title,
    this.author,
    this.description,
    this.guid,
    this.pubDate,
    this.link,
    this.enclosure
  });

 factory FeedItems.fromJson(Map<String, dynamic> json) {
    return FeedItems(
      title: Title.fromJson(json["title"]),
        author: json["author"],
      description: Description.fromJson(json["description"]),  
        guid: json["guid"],
        pubDate: json["pubDate"],
      link: Link.fromJson(json["link"]),
      enclosure: Enclosure.fromJson(json["enclosure"])
    );
  }
}

class Title {
  String title;

  Title ({ this.title });

  factory Title.fromJson(Map<String, dynamic> json) {
    String titleHold = json["\$t"];
    // remove escape characters 
    String temp = titleHold.replaceAll(String.fromCharCode(92), "");
    return Title(title: temp);
  }
}
class Description {
  String description;

  Description ({ this.description });

  factory Description.fromJson(Map<String, dynamic> json) {
    var temp = HtmlTags.removeTag(htmlString:json["__cdata"]);
    return Description(description: temp);
  }
}
class Link {
  String link;

  Link ({ this.link });

  factory Link.fromJson(Map<String, dynamic> json) {
    return Link(link: json["\$t"]);
  }
}
class Enclosure {
  String enclosure;

  Enclosure ({ this.enclosure });

  factory Enclosure.fromJson(Map<String, dynamic> json) {
    return Enclosure(enclosure: json["url"]);
  }
}


class HtmlTags {
  static removeTag({ htmlString, callback }){
    var document = parse(htmlString);
    // remove html tags
    String parsedString = parse(document.body.text).documentElement.text;
    // remove escape characters and return
    return parsedString.replaceAll(String.fromCharCode(92), "");
  }
}


/*
raw data ex.
[
  {
  title: {$t: Deep Foods recalls golden raisins for undeclared sulfites}, 
  author: {$t: shillingr@bnpmedia.com (Rose Shilling)}, 
  description: {__cdata: <p>Deep Foods Inc. recalled Deep golden raisins in plastic bags of three sizes because they contain sulfites that weren\&rsquo;t listed on labels.</p>}, 
  guid: {$t: http://www.foodengineeringmag.com/articles/98322}, 
  pubDate: {$t: Tue, 04 Jun 2019 12:00:00 -0400}, 
  link: {$t: https://www.foodengineeringmag.com/articles/98322-deep-foods-recalls-golden-raisins-for-undeclared-sulfites}, 
  enclosure: {url: https://www.foodengineeringmag.com/ext/resources/eNews/2019/Golden-Raisin-recall_900x550.jpeg?1559672580, 
  type: image/jpeg, length: 82344}}, 

  {title: {$t: Hard plastic piece leads to Jo........
*/