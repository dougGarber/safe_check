// firestore ingredients document
class Ingredients {
  String allergens;
  String cat;
  String id;
  String info;
  String names;
  String nosh;
  String wiki;

  Ingredients ({
    this.allergens,
    this.cat,
    this.id,
    this.info,
    this.names,
    this.nosh,
    this.wiki,
  });

Ingredients.fromMap(Map<dynamic, dynamic> data)
    : allergens = data['allergens'],
      cat = data['cat'],
      id = data['id'],
      info = data['info'],
      names = data['names'], 
      nosh = data['nosh'],
      wiki = data['wiki'];
}

// firestore ingredient types document
class Types {
  String id;
  String ex;
  String name;
  String use;

  Types ({
    this.id,
    this.ex,
    this.name,
    this.use,
  });

Types.fromMap(Map<dynamic, dynamic> data)
    : id = data['id'],
      ex = data['ex'],
      name = data['name'],
      use = data['use'];
}

class ChartCnts {
  String type;
  int cnt;
  
  ChartCnts (
    this.type,
    this.cnt,
  );
}

class Diets {
  String id;

  Diets ({
    this.id,
  });

   Diets.fromMap(Map<dynamic, dynamic> data) 
     : id = data['id'];
  
}