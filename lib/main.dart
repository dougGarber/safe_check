import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/material.dart';
import './utility/safe_check_icons_icons.dart';
import 'pages/tab_home.dart';
import 'pages/tab_safecheck.dart';
import 'pages/tab_ingredients.dart';
import 'utility/globals.dart';
import './services/firestore_service.dart';
//import './services/database_service.dart';

void main() => runApp(MyApp());






class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // index for which page is in tab
  int currentPage = 0;

  GlobalKey bottomNavigationKey = GlobalKey();

  FirestoreMedthods fireObj = FirestoreMedthods();


// init 
  @override
  void initState() {
    fireObj.getDiets().then((results) {
      globals.dietsSnapshot = results;
         print(globals.dietsSnapshot.data);
    });

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: globals.appColor,
        leading: Icon(SafeCheckIcons.logo, size: 35.0, color: globals.logoColor,),
        
      ),
      body: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: Center(
          // Load widget(page)
          child: _getPage(currentPage),
        ),
      ),
      bottomNavigationBar: FancyBottomNavigation(
        barBackgroundColor: globals.appColor,
        circleColor: globals.tabColor,
        activeIconColor: globals.white,
        inactiveIconColor: globals.inactiveIcon,
        tabs: [
          TabData(
            iconData: Icons.home,
            title: "Home",
            onclick: () {
              final FancyBottomNavigationState fState =
                bottomNavigationKey.currentState;
              fState.setPage(0);
            }),
          TabData(
            iconData: SafeCheckIcons.scan,
            title: "Safe Check",
            onclick: () {
              final FancyBottomNavigationState fState=
                bottomNavigationKey.currentState;
              fState.setPage(1);
            }),
          TabData(
            iconData: SafeCheckIcons.ingr, 
            title: "Ingredients",
            onclick: () {
              final FancyBottomNavigationState fState=
                bottomNavigationKey.currentState;
              fState.setPage(2);
            }),
        ],
        initialSelection: 0,   // tab bar init
        key: bottomNavigationKey,
        onTabChangedListener: (position) {
          setState(() {
            currentPage = position;
          });
        },
      ),
     // drawer: Drawer(
     //   child: ListView(
     //     children: <Widget>[Text("Hello"), Text("World")],
     //   ),
     // ),
    );
  }

// Widget(page) Selector
  _getPage(int page) {
    switch (page) {
      case 0:
        return TabHome();
      case 1:
        return TabSafeCheck();
      default:
        return TabIngredients();
    }
  }
}