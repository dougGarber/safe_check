import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Globals {
  static final Globals _globals = new Globals._internal();
  
  DocumentSnapshot dietsSnapshot;

  final appColor = const Color(0xFF799A6B);
  final logoColor = const Color(0xFF5C7E4D);
  final tabColor = const Color(0xFF9EB993);
  final white = const Color(0xFFF7F7F7);
  final inactiveIcon = const Color(0xFFC8D8C1);
  String text;
  
  factory Globals() {
    return _globals;
  }
  Globals._internal();
}
final globals = Globals();